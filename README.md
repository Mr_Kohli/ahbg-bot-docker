# AHBG bot

## Achtung, das Repo wurde archiviert, da es mit dem [Hauptprojekt](https://gitlab.com/Mr_Kohli/ahbg-bot) zusammengeführt wurde

Der ganze Stolz der AHBG Inc.



## Starten

Um den Bot zu starten muss nur der folgende Befehl eingegeben werden:

```bash
python main.py
```

Um alle Befehlen, muss einfach (wenn der Bot bereits in Betrieb ist!) das folgende auf Discord gesendet werden:

````
!help
````



## Setup

<mark>Wichtig!</mark> vor irgendein Skript ausgeführt werden kann, muss sicherstellt werden, dass mindesten Python3.10 installiert sein!

Als erstes muss ein Bot-Token erstellt werden, eine englische Anleitung kann [hier](https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/) gefunden werden (Schritte 2-4). Anschließend muss im "data" Ordern eine Datei mit dem Name "TOKEN.txt" erstellt werden, in welche der Bot-Token eingefügt werden muss. <mark>Es darf nichts anderes in dieser Datei sein!</mark> 



Beim ersten Ausführen des Bots sollten alle benötigten Pakete automatisch installiert, falls das nicht geschieht, können diese mit dem mittels eins Skripts installiert werden:

```
python installer.py
```

oder auch manuell mittels des folgenden Befehls:

```
pip install -r requirements.txt
```

