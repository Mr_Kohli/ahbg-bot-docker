FROM python:latest

WORKDIR /usr/app/src

COPY main.py ./
COPY data ./data
COPY modules ./modules
COPY requirements.txt ./

RUN ["pip" "install" "-r" "requirements.txt"]
RUN ["apt-get", "update"]
RUN ["apt-get", "upgrade", "-y"]
RUN ["apt-get", "install", "nano", "-y"]
CMD [ "python", "./main.py" ]
