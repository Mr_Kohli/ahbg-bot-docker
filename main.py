# Necessary python packages
try:
    import discord

except ModuleNotFoundError as e:
    # ANSI Stuff
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"

    print(f'{pref}0;{error}' + 'Error: ' + reset + 'a required package is not installed!')
    quit(-1)

# Custom modules
try:
    from modules import colors
    from modules import helper
    from modules import imageSelector as IS
    from modules import quoteSelector as QS

except ModuleNotFoundError as e:
    # ANSI Stuff
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"

    print(f'{pref}0;{error}' + 'Error: ' + reset + 'module missing! Please download it from the following gitlab page: '
                                                   'https://gitlab.com/Mr_Kohli/ahbg-bot')
    quit(-1)


# Discord Client
client = discord.Client()


def findCommand(messageSend):
    if any('!meme' in s for s in messageSend):
        return "meme"
    elif any('!zeitung' in s for s in messageSend):
        return "zeitung"
    elif any('!zitat' in s for s in messageSend):
        return "zitat"
    elif any('!help' in s for s in messageSend):
        return "hilfe"


@client.event
async def on_ready():
    logo1 = "           _    _ ____   _____     _____ _   _  _____       ____   ____ _______ \n"
    logo2 = "     /\   | |  | |  _ \ / ____|   |_   _| \ | |/ ____|     |  _ \ / __ \__   __|\n"
    logo3 = "    /  \  | |__| | |_) | |  __      | | |  \| | |          | |_) | |  | | | |   \n"
    logo4 = "   / /\ \ |  __  |  _ <| | |_ |     | | | . ` | |          |  _ <| |  | | | |   \n"
    logo5 = "  / ____ \| |  | | |_) | |__| |    _| |_| |\  | |____ _    | |_) | |__| | | |   \n"
    logo6 = " /_/    \_\_|  |_|____/ \_____|   |_____|_| \_|\_____(_)   |____/ \____/  |_|   \n"
    print(f'{colors.pref}0;{colors.logo}' + logo1 + logo2 + logo3 + logo4 + logo5 + logo6 + colors.reset)
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=' sich die neue HTL Krone an'))
    print('Bot is now ' + f'{colors.pref}0;{colors.green}' + 'online' + colors.reset + ' as ' + f'{colors.pref}0;{colors.user}' +
          '{0.user}'.format(client) + colors.reset)


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    messageSend = message.content.lower()
    messageSend = messageSend.split(' ')

    command = findCommand(messageSend)

    match command:
        case "meme":
            f = open(IS.randMeme(), "rb")
            meme = discord.File(f)

            msg = "Einer der sehr sehr guten Memes wird gesucht."
            await message.channel.send(msg)
            await message.channel.send(file=meme)

            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " will Maimais sehen")

        case "zeitung":
            f = open(IS.randZeit(), "rb")
            zeitung = discord.File(f)

            msg = "Suche die guhte Fachlektüre"
            await message.channel.send(msg)
            await message.channel.send(file=zeitung)

            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " braucht etwas mentale Stimulation")

        case "zitat":
            zitat = QS.randQuote()
            msg = "```" + zitat + "```"
            await message.channel.send(msg)

            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " wollte das folgende Zitat sehen:\n"
                  + f'{colors.pref}0;{colors.message}' + zitat + colors.reset)

        case "hilfe":
            msg = helper.generateHelp()
            await message.channel.send(msg)

            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " braucht dringent Hilfe\n")


if __name__ == '__main__':
    try:
        # Reads Bot-Token from file
        token = open("data/TOKEN.txt", "r")
        # Starts bot
        client.run(str(token.read()))
    except FileNotFoundError as e:
        print(f'{colors.pref}0;{colors.error}' + 'Error: ' + colors.reset + 'TOKEN.txt not found.\n'
              + 'Please see the README for help')
        quit(-1)
    except Exception as e:
        print(f'{colors.pref}0;{colors.error}' + 'Error: ' + colors.reset + 'the following error has occurred:\n' +
              str(e) + '\n\nThe program will now be terminated')
        quit(-1)
