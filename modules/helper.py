def generateHelp():
    msg = "Der wichtigste Bot auf dem Server! \n\nEr kann das folgende:\n" \
          "```!meme```" \
          "sendet einen der qualitäts Memes der Klasse.\n" \
          "```!zeitung```" \
          "sendet ein Exemplar der HTL Krone oder des Lenzets"\
          "```!zitat```" \
          "sendet eines der Zitate aus dem Zitatekanal.\n" \
          "```!help```" \
          "sendet das hier (warum auch immer man das erklären muss).\n\n\n" \
          "Bei Fragen oder Problemen melden Sie ich bei dem Medienwart ihres Vertrauen."

    return msg
