from random import randint


def loadQuotes():
    f = open("data/zitate.csv", "r", encoding="utf8")

    zitate = f.read()
    zitate = zitate.split("\n")

    return zitate


def randQuote():
    zitateListe = loadQuotes()
    numZitate = len(zitateListe)

    rand = randint(0, (numZitate-1))
    zitat = zitateListe[rand]
    zitat = zitat.replace("\ß", "\n")

    return zitat
